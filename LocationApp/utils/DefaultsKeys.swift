//
//  DefaultsKeys.swift
//  LocationApp
//
//  Created by Mac on 01/09/2021.
//

import Foundation

struct DefaultsKeys {
    static let phoneNumber = "phoneNumber"
    static let cmnd  = "cmnd"
    static let fullname = "fullname"
    static let radius = "radius"
    static let sex = "sex"
    static let province = "province"
    static let district = "district"
    static let ward = "ward"
    static let address = "address"
    static let mgs = "mgs"
    static let registerUser = "registerUser"
    static let locationLatitude = "locationLatitude"
    static let locationLongitude = "locationLongitude"
    static let idUser = "idUser"
}
