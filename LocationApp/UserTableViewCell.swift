//
//  UserTableViewCell.swift
//  LocationApp
//
//  Created by Mac on 04/09/2021.
//

import UIKit

protocol TableViewNew {
    func onClickResetCell(index: Int)
    func onClickPowerCell(index: Int)
}

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var labelFullname: UILabel!
    @IBOutlet weak var labelPhoneNumber: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    var cellDelegate: TableViewNew?
    var index: IndexPath?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnPowerAction(_ sender: Any) {
        cellDelegate?.onClickPowerCell(index: index!.row)
    }
    
    @IBAction func btnResetAction(_ sender: Any) {
        cellDelegate?.onClickResetCell(index: index!.row)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
