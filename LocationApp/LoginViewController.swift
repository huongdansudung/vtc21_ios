//
//  LoginViewController.swift
//  LocationApp
//
//  Created by Mac on 03/09/2021.
//

import UIKit
import SwiftUI
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import Firebase

class LoginViewController: UIViewController {
    @IBOutlet weak var layoutHeaderLogin: UIView!
    @IBOutlet weak var layoutFromLogin: UIView!
    @IBOutlet weak var textFieldPassword: MDCFilledTextField!
    @IBOutlet weak var textFieldUsername: MDCFilledTextField!
    var defaults = UserDefaults.standard
    var isCheckBox = false
    
    @IBOutlet weak var singleCheckBoxOutlet: UIButton!{
        didSet{
            singleCheckBoxOutlet.setImage(UIImage(named: "uncheckbox"), for: .normal)
            singleCheckBoxOutlet.setImage(UIImage(named: "checkbox"), for: .selected)
            singleCheckBoxOutlet.layer.cornerRadius = singleCheckBoxOutlet.frame.height / 2
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        if (textFieldPassword.text == "Vbpo@123") {
            if (isCheckBox == true) {
                defaults.setValue(textFieldUsername.text, forKey: "Username")
                defaults.setValue(textFieldPassword.text, forKey: "Password")
            }
            
           
            Messaging.messaging().token { [self] token, error in
              if let error = error {
                print("Error fetching FCM registration token: \(error)")
              } else if let token = token {
                print("FCM registration token: \(token)")
                MGConnection.request(APIRouter.registerToken(
                                        phone: textFieldUsername.text!,
                                        deviceToken: "IOS/\(token)"),
                                     RegisterTokenResponse.self,
                                     completion: {(result, err) in
                                        guard err == nil else {
                                            Toast.show(message: (err?.mErrorMessage)!, controller: self)
                                            return
                                        }
                                        DispatchQueue.main.async {
                                           if result?.message == "Success" {
                                                // điều hướng và save mgs
                                                self.defaults.setValue(textFieldUsername.text, forKey: DefaultsKeys.mgs)
                                                self.navigarionToUserCheckerScreen()
                                           }else {
                                                Toast.show(message: "Đăng nhập thất bại.", controller: self)
                                           }
                                        }
                                    })

              }
            }
        }else {
            Toast.show(message: "Mã giám sát hoặc mật khẩu không đúng", controller: self)
        }
    }
    
    @IBAction func singleCheckboxAction(_ sender: UIButton) {
        sender.checkboxAnimation { [self] in
            print("I'm done")
            print(sender.isSelected)
            self.isCheckBox = sender.isSelected
            if (sender.isSelected == true) {
                defaults.setValue("true", forKey: "remember")
            }else if (sender.isSelected == false) {
                defaults.setValue("false", forKey: "remember")
            }
        }
    }
    
    private func initView() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if (defaults.string(forKey: "remember") == "true") {
            singleCheckBoxOutlet.setImage(UIImage(named: "checkbox"), for: .normal)
            textFieldUsername.text = defaults.string(forKey: "Username")
            textFieldPassword.text = defaults.string(forKey: "Password")
        }
        layoutHeaderLogin.roundCorners([.bottomLeft, .bottomRight], radius: 18)
        layoutFromLogin.roundCorners([.bottomLeft, .bottomRight, .topLeft, .topRight], radius: 16)
        textFieldUsername.label.text = "Mã người giám sát"
        textFieldUsername.setFilledBackgroundColor(UIColor.white, for: MDCTextControlState.normal)
        textFieldUsername.setFilledBackgroundColor(UIColor.white, for: MDCTextControlState.editing)
        textFieldUsername.setNormalLabelColor(UIColor.black, for: MDCTextControlState.normal)
        textFieldUsername.setFloatingLabelColor(UIColor.black, for: MDCTextControlState.normal)
        textFieldUsername.setUnderlineColor(UIColor.black, for: MDCTextControlState.normal)
        textFieldUsername.setTextColor(UIColor.black, for: MDCTextControlState.editing)
        textFieldUsername.setTextColor(UIColor.black, for: MDCTextControlState.normal)
        textFieldUsername.setUnderlineColor(UIColor(named: "Green")!, for: MDCTextControlState.editing)
        textFieldUsername.setFloatingLabelColor(UIColor(named: "Green")!, for: MDCTextControlState.editing)
        textFieldUsername.sizeToFit()
        textFieldPassword.label.text = "Mật khẩu"
        textFieldPassword.setFilledBackgroundColor(UIColor.white, for: MDCTextControlState.normal)
        textFieldPassword.setFilledBackgroundColor(UIColor.white, for: MDCTextControlState.editing)
        textFieldPassword.setNormalLabelColor(UIColor.black, for: MDCTextControlState.normal)
        textFieldPassword.setFloatingLabelColor(UIColor.black, for: MDCTextControlState.normal)
        textFieldPassword.setUnderlineColor(UIColor.black, for: MDCTextControlState.normal)
        textFieldPassword.setTextColor(UIColor.black, for: MDCTextControlState.editing)
        textFieldPassword.setTextColor(UIColor.black, for: MDCTextControlState.normal)
        textFieldPassword.setUnderlineColor(UIColor(named: "Green")!, for: MDCTextControlState.editing)
        textFieldPassword.setFloatingLabelColor(UIColor(named: "Green")!, for: MDCTextControlState.editing)
        textFieldPassword.sizeToFit()
    }
    
    private func navigarionToUserCheckerScreen() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let userCheckerScreen = sb.instantiateViewController(identifier: "UserChecker")
            as! UsersViewController
        self.navigationController?.pushViewController(userCheckerScreen, animated: true)
    }
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11, *) {
            var cornerMask = CACornerMask()
            if(corners.contains(.topLeft)){
                cornerMask.insert(.layerMinXMinYCorner)
            }
            if(corners.contains(.topRight)){
                cornerMask.insert(.layerMaxXMinYCorner)
            }
            if(corners.contains(.bottomLeft)){
                cornerMask.insert(.layerMinXMaxYCorner)
            }
            if(corners.contains(.bottomRight)){
                cornerMask.insert(.layerMaxXMaxYCorner)
            }
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = cornerMask

        } else {
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
}

extension UIButton {
    func checkboxAnimation(closure: @escaping () -> Void){
        guard let image = self.imageView else {return}
        self.adjustsImageWhenHighlighted = false
        self.isHighlighted = false
        
        UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
            image.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            
        }) { (success) in
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
                self.isSelected = !self.isSelected
                closure()
                image.transform = .identity
            }, completion: nil)
        }
        
    }
}
