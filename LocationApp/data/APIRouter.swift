//
//  APIRouter.swift
//  LocationApp
//
//  Created by Mac on 23/08/2021.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    
    // =========== Begin define api ===========
    case createUser(id: String,
                    name: String,
                    cmnd: String,
                    adress: String,
                    tinh: String,
                    quan: String,
                    phuong: String,
                    telephone: String,
                    location: String,
                    rarius: String,
                    mags: String,
                    is_lock: String,
                    is_reset: String)
    case getUsers(mgs: String)
    case lockUser(id: String,
                  name: String,
                  cmnd: String,
                  adress: String,
                  tinh: String,
                  quan: String,
                  phuong: String,
                  telephone: String,
                  location: String,
                  rarius: String,
                  mags: String,
                  is_lock: String,
                  is_reset: String)
    case resetUser(id: String,
                   name: String,
                   cmnd: String,
                   adress: String,
                   tinh: String,
                   quan: String,
                   phuong: String,
                   telephone: String,
                   location: String,
                   rarius: String,
                   mags: String,
                   is_lock: String,
                   is_reset: String)
    case getLogs(id: String)
    case requestLocation(id: String,
                         name: String,
                         cmnd: String,
                         adress: String,
                         tinh: String,
                         quan: String,
                         phuong: String,
                         telephone: String,
                         location: String,
                         rarius: String,
                         mags: String,
                         is_lock: String,
                         is_reset: String)
    case callFo(id: String)
    case editLocation(id: String, location: String)
    case registerToken(phone: String, deviceToken: String)
    
    // =========== End define api ===========
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .createUser, .getUsers, .lockUser, .resetUser, .getLogs, .requestLocation, .callFo, .editLocation, .registerToken:
            return .post
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .createUser:
            return "creater_user"
        case .getUsers:
            return "request_qly"
        case .lockUser:
            return "lock_user"
        case .resetUser:
            return "reset_user"
        case .getLogs:
            return "get_log"
        case .requestLocation:
            return "request"
        case .callFo:
            return "call_F0"
        case .editLocation:
            return "edit_user"
        case .registerToken:
            return "registerToken"
        }
        
    }
        
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .createUser(let id,
                         let name,
                         let cmnd,
                         let adress,
                         let tinh,
                         let quan,
                         let phuong,
                         let telephone,
                         let location,
                         let rarius,
                         let mags,
                         let is_lock,
                         let is_reset):
            let parameters: [String: Any] = [
                "id": id,
                "name": name,
                "cmnd": cmnd,
                "adress": adress,
                "tinh": tinh,
                "quan": quan,
                "phuong": phuong,
                "telephone": telephone,
                "location": location,
                "rarius": rarius,
                "mags": mags,
                "is_lock": is_lock,
                "is_reset": is_reset
            ]
            return parameters
        case .getUsers(let telephone):
            let parameters: [String: Any] = [
                "telephone": telephone
            ]
            return parameters
        case .lockUser(let id,
                       let name,
                       let cmnd,
                       let adress,
                       let tinh,
                       let quan,
                       let phuong,
                       let telephone,
                       let location,
                       let rarius,
                       let mags,
                       let is_lock,
                       let is_reset):
          let parameters: [String: Any] = [
              "id": id,
              "name": name,
              "cmnd": cmnd,
              "adress": adress,
              "tinh": tinh,
              "quan": quan,
              "phuong": phuong,
              "telephone": telephone,
              "location": location,
              "rarius": rarius,
              "mags": mags,
              "is_lock": is_lock,
              "is_reset": is_reset
          ]
          return parameters
        case .resetUser(let id,
                        let name,
                        let cmnd,
                        let adress,
                        let tinh,
                        let quan,
                        let phuong,
                        let telephone,
                        let location,
                        let rarius,
                        let mags,
                        let is_lock,
                        let is_reset):
           let parameters: [String: Any] = [
               "id": id,
               "name": name,
               "cmnd": cmnd,
               "adress": adress,
               "tinh": tinh,
               "quan": quan,
               "phuong": phuong,
               "telephone": telephone,
               "location": location,
               "rarius": rarius,
               "mags": mags,
               "is_lock": is_lock,
               "is_reset": is_reset
           ]
           return parameters
        case .getLogs(let id):
            let parameters: [String: Any] = [
                "id": id
            ]
            return parameters
        case .requestLocation(let id,
                              let name,
                              let cmnd,
                              let adress,
                              let tinh,
                              let quan,
                              let phuong,
                              let telephone,
                              let location,
                              let rarius,
                              let mags,
                              let is_lock,
                              let is_reset):
                 let parameters: [String: Any] = [
                     "id": id,
                     "name": name,
                     "cmnd": cmnd,
                     "adress": adress,
                     "tinh": tinh,
                     "quan": quan,
                     "phuong": phuong,
                     "telephone": telephone,
                     "location": location,
                     "rarius": 0,
                     "mags": mags,
                     "is_lock": 0,
                     "is_reset": 0
                 ]
                 return parameters
        case .callFo(let id):
            let parameters: [String: Any] = [
                "id": id,
                "name": "name",
                "cmnd": "cmnd",
                "adress": "adress",
                "tinh": "tinh",
                "quan": "quan",
                "phuong": "phuong",
                "telephone": "telephone",
                "location": "location",
                "rarius": 0,
                "mags": "mags",
                "is_lock": 0,
                "is_reset": 0
            ]
            return parameters
        case .editLocation(let id, let location):
            let parameters: [String: Any] = [
                "id": id,
                "name": "name",
                "cmnd": "cmnd",
                "adress": "adress",
                "tinh": "tinh",
                "quan": "quan",
                "phuong": "phuong",
                "telephone": "telephone",
                "location": location,
                "rarius": 0,
                "mags": "mags",
                "is_lock": 0,
                "is_reset": 0
            ]
            return parameters
        case .registerToken(let phone,let deviceToken):
            let parameters: [String: Any] = [
                "phone": phone,
                "deviceToken": deviceToken
            ]
            print(parameters)
            return parameters
        }
    }
    
    private func getAuthorizationHeader(isRefresh: Bool) -> String? {
        return ""
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Production.BASE_URL.asURL()
        // setting path
        var urlRequest: URLRequest = URLRequest(url: url.appendingPathComponent(path))
        // setting method
        urlRequest.httpMethod = method.rawValue
        // setting header
        if let parameters = parameters {
            do {
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            } catch {
                print("Encoding fail")
            }
        }

        return urlRequest
    }
}
