//
//  District.swift
//  LocationApp
//
//  Created by Mac on 25/08/2021.
//

import Foundation
import ObjectMapper

class District: Mappable {
    var district_id: String?
    var district_name: String?
    var district_type: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        district_id <- map["district_id"]
        district_name <- map["district_name"]
        district_type <- map["district_type"]
    }
}
