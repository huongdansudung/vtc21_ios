//
//  Log.swift
//  LocationApp
//
//  Created by Mac on 04/09/2021.
//

import Foundation
import ObjectMapper

class Log: Mappable {
    var date_chance: String?
    var log: String?
    var location: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        date_chance <- map["date_chance"]
        log <- map["log"]
        location <- map["location"]
    }
}
