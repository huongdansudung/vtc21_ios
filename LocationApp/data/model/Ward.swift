//
//  Ward.swift
//  LocationApp
//
//  Created by Mac on 25/08/2021.
//

import Foundation
import ObjectMapper

class Ward: Mappable {
    var ward_id: String?
    var ward_name: String?
    var ward_type: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        ward_id <- map["ward_id"]
        ward_name <- map["ward_name"]
        ward_type <- map["ward_type"]
    }
}
