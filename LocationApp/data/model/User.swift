//
//  User.swift
//  LocationApp
//
//  Created by Mac on 04/09/2021.
//

import Foundation
import ObjectMapper

class User: Mappable {
    var id: Int?
    var name: String?
    var adress: String?
    var tinh: String?
    var quan: String?
    var phuong: String?
    var telephone: String?
    var location: String?
    var rarius: Int?
    var mags: String?
    var is_lock: Int?
    var is_reset: Int?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        adress <- map["adress"]
        tinh <- map["tinh"]
        adress <- map["adress"]
        quan <- map["quan"]
        phuong <- map["phuong"]
        telephone <- map["telephone"]
        location <- map["location"]
        mags <- map["mags"]
        is_lock <- map["is_lock"]
        is_reset <- map["is_reset"]
    }
}
