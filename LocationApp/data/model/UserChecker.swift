//
//  UserChecker.swift
//  LocationApp
//
//  Created by Mac on 04/09/2021.
//
import Foundation
import ObjectMapper

class UserChecker: Mappable {
    var id: Int?
    var name: String?
    var telephone: String?
    var date_start: String?
    var log: String?
    var count_call: Int?
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        telephone <- map["telephone"]
        date_start <- map["date_start"]
        log <- map["log"]
        count_call <- map["count_call"]
    }
}
