//
//  Province.swift
//  LocationApp
//
//  Created by Mac on 24/08/2021.
//

import Foundation
import ObjectMapper

class Province: Mappable {
    var province_id: String?
    var province_name: String?
    var province_type: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        province_id <- map["province_id"]
        province_name <- map["province_name"]
        province_type <- map["province_type"]
    }
}
