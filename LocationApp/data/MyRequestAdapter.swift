//
//  MyRequestAdapter.swift
//  LocationApp
//
//  Created by Mac on 23/08/2021.
//

import Foundation
import Alamofire
import GTAlertCollection

class MyRequestAdapter: RequestRetrier, RequestAdapter {
    private var isRefreshing = false
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?) -> Void
    static let shared = MyRequestAdapter()
    let defaults = UserDefaults.standard
    
    private let lock = NSLock()
    private var requestsToRetry: [RequestRetryCompletion] = []
    var accessToken:String? = nil
    var refreshToken:String? = nil
    
    private init(){
       let sessionManager = Alamofire.SessionManager.default
       sessionManager.adapter = self
       sessionManager.retrier = self
   }

    //http://192.168.0.200:5050/
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        if let urlString = urlRequest.url?.absoluteString,
           urlString.hasPrefix("http://192.168.0.200:5050/"),
           !urlString.hasSuffix("refresh") {
            if let token = accessToken {
                urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
        }
        return urlRequest
    }
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        let defaults = UserDefaults.standard
   
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)

            if !isRefreshing {
            }
        } else {
            completion(false, 0.0)
        }
    }
    
}
