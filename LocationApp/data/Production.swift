//
//  Production.swift
//  LocationApp
//
//  Created by Mac on 23/08/2021.
//

import Foundation

struct Production {
    static let BASE_URL: String = "http://113.160.226.174:6113/"
}

enum NetworkErrorType {
    case API_ERROR
    case HTTP_ERROR
    case NETWORK_ERROR
}
