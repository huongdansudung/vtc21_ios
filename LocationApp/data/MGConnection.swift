//
//  MGConnection.swift
//  LocationApp
//
//  Created by Mac on 23/08/2021.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class MGConnection {
    static let sessionManager = Alamofire.SessionManager.default
    
    static func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    static func request<T: Mappable>(_ apiRouter: APIRouter,_ returnType: T.Type, completion: @escaping (_ result: T?, _ error: BaseResponseError?) -> Void) {
        sessionManager.adapter = MyRequestAdapter.shared
        if !isConnectedToInternet() {
            let err: BaseResponseError = BaseResponseError.init(NetworkErrorType.NETWORK_ERROR, 402, "Không tìm thấy server, Vui lòng kiểm tra lại internet!")
            completion(nil, err)
            return
        }
        
        sessionManager.request(apiRouter).validate().responseObject {(response: DataResponse<T>) in
            switch response.result {
            case .success:
                if response.response?.statusCode == 200 {
                    completion((response.result.value), nil)
                } else {
                    print("error \(response.response?.statusCode)!")
                    let err: BaseResponseError = BaseResponseError.init(NetworkErrorType.HTTP_ERROR, (response.response?.statusCode)!, "Request is error!")
                    completion(nil, err)
                }
                
                break
                
            case .failure(let error):
                if error is AFError {
                    let err: BaseResponseError = BaseResponseError.init(NetworkErrorType.HTTP_ERROR, response.response!.statusCode, "Request is error!")
                    completion(nil, err)
                }
                
                break
            }
        }
    }
    
}
