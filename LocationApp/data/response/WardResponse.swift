//
//  WardResponse.swift
//  LocationApp
//
//  Created by Mac on 25/08/2021.
//

import Foundation
import ObjectMapper

class WardResponse: Mappable {
    var results: [Ward]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        results <- map["results"]
    }
}
