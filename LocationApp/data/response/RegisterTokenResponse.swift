//
//  RegisterTokenResponse.swift
//  LocationApp
//
//  Created by Mac on 30/09/2021.
//

import Foundation
import ObjectMapper

class RegisterTokenResponse: Mappable {
    var message: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        message <- map["message"]
    }
}
