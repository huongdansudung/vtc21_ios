//
//  LogsResponse.swift
//  LocationApp
//
//  Created by Mac on 04/09/2021.
//

import Foundation
import ObjectMapper

class LogsResponse: Mappable {
    var logs: [Log]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        logs <- map["log"]
    }
}
