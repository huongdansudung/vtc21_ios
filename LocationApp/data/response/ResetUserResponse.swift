//
//  ResetUserResponse.swift
//  LocationApp
//
//  Created by Mac on 04/09/2021.
//

import Foundation
import ObjectMapper

class ResetUserResponse: Mappable {
    var message: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        message <- map["Messenger"]
    }
}
