//
//  CreateUserResponse.swift
//  LocationApp
//
//  Created by Mac on 01/09/2021.
//

import Foundation
import ObjectMapper

class CreateUserResponse: Mappable {
    var user: User?
    var message: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        user <- map["user"]
        message <- map["Messenger"]
    }
}
