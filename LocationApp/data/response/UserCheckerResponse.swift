//
//  UserCheckerResponse.swift
//  LocationApp
//
//  Created by Mac on 04/09/2021.
//

import Foundation
import ObjectMapper

class UserCheckerResponse: Mappable {
    var users: [UserChecker]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        users <- map["user"]
    }
}
