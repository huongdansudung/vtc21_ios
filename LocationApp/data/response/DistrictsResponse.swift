//
//  DistrictsResponse.swift
//  LocationApp
//
//  Created by Mac on 25/08/2021.
//

import Foundation
import ObjectMapper

class DistrictsResponse: Mappable {
    var results: [District]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        results <- map["results"]
    }
}
