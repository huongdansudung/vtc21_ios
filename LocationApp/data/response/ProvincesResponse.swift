//
//  ProvincesResponse.swift
//  LocationApp
//
//  Created by Mac on 24/08/2021.
//

import Foundation
import ObjectMapper

class ProvincesResponse: Mappable {
    var results: [Province]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        results <- map["results"]
    }
}
