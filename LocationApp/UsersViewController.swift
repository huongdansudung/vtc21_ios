//
//  UsersViewController.swift
//  LocationApp
//
//  Created by Mac on 04/09/2021.
//

import UIKit

class UsersViewController: UIViewController {
    @IBOutlet weak var userTableView: UITableView!
    var users: [UserChecker]? = nil
    var defaults = UserDefaults.standard
    let refreshControl = UIRefreshControl()
    
    @IBOutlet weak var labelTitleToolbar: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        getUsers(mgs: defaults.string(forKey: DefaultsKeys.mgs)!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        defaults.setValue("", forKey: DefaultsKeys.mgs)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func initView() {
        refreshControl.attributedTitle = NSAttributedString(string: "Làm mới")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        labelTitleToolbar.text = "Danh sách của \(defaults.string(forKey: DefaultsKeys.mgs)!)"
        userTableView.dataSource = self
        userTableView.delegate = self
        userTableView.refreshControl = refreshControl
    }
    
    private func navigationToMapCheckerScreen(id: String, username: String) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let mapScreen = sb.instantiateViewController(identifier: "MapChecker") as! MapCheckerViewController
        mapScreen.idUser = id
        mapScreen.username = username
        self.navigationController?.pushViewController(mapScreen, animated: true)
    }
    
    private func getUsers(mgs: String) {
        MGConnection.request(APIRouter.getUsers(mgs: mgs), UserCheckerResponse.self, completion: {(result, err) in
            guard err == nil else {
                Toast.show(message: (err?.mErrorMessage)!, controller: self)
                return
            }
            DispatchQueue.main.async {
                self.users = result?.users
                self.userTableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        })
    }
    
    private func resetUser(id: String) {
        print("id reset \(id)")
        MGConnection.request(APIRouter.resetUser(id: id,
                                                 name: "name",
                                                 cmnd: "cmnd",
                                                 adress: "adress",
                                                 tinh: "tinh",
                                                 quan: "quan",
                                                 phuong: "phuong",
                                                 telephone: "telephone",
                                                 location: "telephone",
                                                 rarius: "0",
                                                 mags: "mags",
                                                 is_lock: "0",
                                                 is_reset: "1"),
                             ResetUserResponse.self,
                             completion: {(result, err) in
                             guard err == nil else {
                                 Toast.show(message: (err?.mErrorMessage)!, controller: self)
                                 return
                             }
                                DispatchQueue.main.async { [self] in
                                if result?.message == "Ok" {
                                    self.getUsers(mgs: defaults.string(forKey: DefaultsKeys.mgs)!)
                                }else {
                                    Toast.show(message: "Reset thất bại.", controller: self)
                                }
                             }
                         })
    }
    
    private func lockUser(id: String) {
        MGConnection.request(APIRouter.lockUser(id: id,
                                                name: "null",
                                                cmnd: "null",
                                                adress: "null",
                                                tinh: "null",
                                                quan: "null",
                                                phuong: "null",
                                                telephone: "null",
                                                location: "null",
                                                rarius: "0",
                                                mags: "null",
                                                is_lock: "1",
                                                is_reset: "0"),
                             ResetUserResponse.self,
                             completion: {(result, err) in
                                guard err == nil else {
                                    Toast.show(message: (err?.mErrorMessage)!, controller: self)
                                    return
                                }
                                DispatchQueue.main.async {
                                   if result?.message == "Ok" {
                                    self.getUsers(mgs: self.defaults.string(forKey: DefaultsKeys.mgs)!)
                                   }else {
                                       Toast.show(message: "Lock thất bại.", controller: self)
                                   }
                                }
                            })
    }
    
    @objc func refresh() {
        getUsers(mgs: self.defaults.string(forKey: DefaultsKeys.mgs)!)
    }
    
}

extension UsersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if users == nil {
            return 0
        }else {
            return users!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserTableViewCell
        cell.labelFullname.text = users![indexPath.row].name
        cell.labelPhoneNumber.text = users![indexPath.row].telephone
        cell.labelDate.text = "Ngày ĐK: \(users![indexPath.row].date_start?.split(separator: "T")[0] ?? "...")"
        cell.index = indexPath
        cell.cellDelegate = self
        
        // Điều kiện ngày
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let array = formatter.string(from: date).split(separator: "-")
        if Int((users![indexPath.row].date_start?.split(separator: "T")[0].split(separator: "-")[1])!) == Int(array[1])
        {
            let a = Int(array[1])!
            let b = Int((users![indexPath.row].date_start?.split(separator: "T")[0].split(separator: "-")[2])!)
            let result =  a - b!
            if result > 14 {
                cell.backgroundColor = UIColor(named: "OverCheck")!
            }else {
                if users![indexPath.row].log == "Rời khỏi khu vực cách ly" || users![indexPath.row].count_call! > 1 {
                    cell.backgroundColor = UIColor(named: "Warning")!
                }else {
                    cell.backgroundColor = UIColor.white
                    cell.labelFullname.textColor = UIColor.black
                    cell.labelPhoneNumber.textColor = UIColor.black
                    cell.labelDate.textColor = UIColor.black
                }
            }
        }else {
            cell.backgroundColor = UIColor(named: "OverCheck")!
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(users![indexPath.row].id!)
        navigationToMapCheckerScreen(id: "\(users![indexPath.row].id!)", username: users![indexPath.row].name!)
    }
}

extension UsersViewController: TableViewNew {
    func onClickResetCell(index: Int) {
        let alert = UIAlertController(title: "Thông báo",
                                      message: "Bạn có muốn làm mới thông tin \(users![index].name!)?",
                                      preferredStyle: .alert
        )
        let cancelAction = UIAlertAction(title: "Không", style: .default, handler: {_ in
           
        })
        let agreeAction = UIAlertAction(title: "Có", style: .default, handler: { [weak self] _ in
            self?.resetUser(id: "\(self!.users![index].id!)")
        })
        alert.addAction(cancelAction)
        alert.addAction(agreeAction)
        present(alert, animated: true, completion: nil)
    }
    
    func onClickPowerCell(index: Int) {
        let alert = UIAlertController(title: "Thông báo",
                                      message: "Bạn có muốn tắt giám sát \(users![index].name!)?",
                                      preferredStyle: .alert
        )
        let cancelAction = UIAlertAction(title: "Không", style: .default, handler: {_ in
            alert.dismiss(animated: true, completion: nil)
        })
        let agreeAction = UIAlertAction(title: "Có", style: .default, handler: { [weak self] _ in
            self?.lockUser(id: "\(self!.users![index].id!)")
        })
        alert.addAction(cancelAction)
        alert.addAction(agreeAction)
        present(alert, animated: true, completion: nil)
    }
}
