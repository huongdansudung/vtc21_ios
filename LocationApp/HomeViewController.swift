//
//  HomeViewController.swift
//  LocationApp
//
//  Created by Mac on 24/08/2021.
//
import UIKit
import MaterialComponents.MaterialTextControls_FilledTextAreas
import MaterialComponents.MaterialTextControls_FilledTextFields
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import MaterialComponents
import RadioGroup
import Alamofire
import MaterialComponents.MaterialButtons

class HomeViewController: UIViewController {
    
    var textFieldsPhoneController: MDCTextInputControllerOutlined?
    var textFieldsCMNDController: MDCTextInputControllerOutlined?
    var textFieldsFullNameController: MDCTextInputControllerOutlined?
    var textFieldsScopeController: MDCTextInputControllerOutlined?
    var textFieldProvinceController: MDCTextInputControllerOutlined?
    var textFieldDistrictController: MDCTextInputControllerOutlined?
    var textFieldWardsController: MDCTextInputControllerOutlined?
    var textFieldAddressController: MDCTextInputControllerOutlined?
    var textFieldMGSController: MDCTextInputControllerOutlined?
    
    @IBOutlet weak var textFieldPhone: MDCTextField!
    @IBOutlet weak var textFieldCMND: MDCTextField!
    @IBOutlet weak var textFieldFullName: MDCTextField!
    @IBOutlet weak var textFieldScope: MDCTextField!
    @IBOutlet weak var radioGroup: RadioGroup!
    @IBOutlet weak var textFieldsProvinces: MDCTextField!
    @IBOutlet weak var textFieldDistricts: MDCTextField!
    @IBOutlet weak var textFieldWards: MDCTextField!
    @IBOutlet weak var textFieldAddress: MDCTextField!
    @IBOutlet weak var textFieldMGS: MDCTextField!
    @IBOutlet weak var buttonNext: UIButton!
    
    var provinces: [Province] = []
    let provincesPicker = UIPickerView()
    var provinceSelect: Province? = nil
    var districts: [District] = []
    let districtsPicker = UIPickerView()
    var districtSelect: District? = nil
    var wards: [Ward] = []
    let wardsPicker = UIPickerView()
    var wardSelect: Ward? = nil
    
    var scopes = ["30m", "40m", "50m", "60m"]
    let scopePicker = UIPickerView()
    var scopeSelect = "30"
    var sexRadioGroupSelect = "Nam"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        getProvinces()
        createScopePicker()
        createProvincesPicker()
        createDistrictsPicker()
        createWardsPicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func actionButtonNext(_ sender: UIButton) {
        if textFieldsPhoneController?.errorText == nil &&
            textFieldPhone.text != "" &&
            textFieldsCMNDController?.errorText == nil &&
            textFieldCMND.text != "" &&
            textFieldsFullNameController?.errorText == nil &&
            textFieldFullName.text != "" &&
            provinceSelect != nil &&
            districtSelect != nil &&
            wardSelect != nil &&
            textFieldAddress.text != "" &&
            textFieldMGS.text != ""{
            saveUser(phoneNumber: textFieldPhone.text!,
                     cmnd: textFieldCMND.text!,
                     fullname: textFieldFullName.text!,
                     radius: scopeSelect,
                     sex: sexRadioGroupSelect,
                     province: (provinceSelect?.province_name)!,
                     district: (districtSelect?.district_name)!,
                     ward: (wardSelect?.ward_name)!,
                     address: textFieldAddress.text!,
                     mgs: textFieldMGS.text!)
            navigationToMapController()
        }else {
            Toast.show(message: "Nhập sai hoặc không đẩy đủ thông tin", controller: self)
        }
       
    }
    
    private func initView() {
        buttonNext.alignImageRight()
        
        textFieldPhone.placeholderLabel.text = "Số điện thoại"
        textFieldsPhoneController = MDCTextInputControllerOutlined(textInput: textFieldPhone)
        textFieldsPhoneController?.characterCountMax = 10
        textFieldsPhoneController?.helperText = "Yêu cầu *"
        textFieldsPhoneController?.leadingUnderlineLabelTextColor = UIColor.red
        textFieldPhone.delegate = self

        textFieldCMND.placeholderLabel.text = "Số CMND/CCCD"
        textFieldsCMNDController = MDCTextInputControllerOutlined(textInput: textFieldCMND)
        textFieldsCMNDController?.characterCountMax = 12
        textFieldsCMNDController?.helperText = "Yêu cầu *"
        textFieldsCMNDController?.leadingUnderlineLabelTextColor = UIColor.red
        textFieldCMND.delegate = self

        textFieldFullName.placeholderLabel.text = "Họ và tên"
        textFieldsFullNameController = MDCTextInputControllerOutlined(textInput: textFieldFullName)
        textFieldsFullNameController?.helperText = "Yêu cầu *"
        textFieldsFullNameController?.leadingUnderlineLabelTextColor = UIColor.red
        textFieldFullName.delegate = self
        
        textFieldAddress.placeholderLabel.text = "Địa chỉ cụ thể"
        textFieldAddressController = MDCTextInputControllerOutlined(textInput: textFieldAddress)
        textFieldAddressController?.helperText = "Yêu cầu *"
        textFieldAddressController?.leadingUnderlineLabelTextColor = UIColor.red
        textFieldAddress.delegate = self
        
        radioGroup.titles = ["Nam", "Nữ"]
        radioGroup.selectedIndex = 0
        radioGroup.addTarget(self, action: #selector(optionSelected), for: .valueChanged)
        
        textFieldMGS.placeholderLabel.text = "Mã người giám sát"
        textFieldMGSController = MDCTextInputControllerOutlined(textInput: textFieldMGS)
        textFieldMGSController?.helperText = "Yêu cầu *"
        textFieldMGSController?.leadingUnderlineLabelTextColor = UIColor.red
        textFieldMGS.delegate = self
    }
    
    private func createScopePicker() {
        let toolbar = UIToolbar()
        scopePicker.tag = 0
        scopePicker.dataSource = self
        scopePicker.delegate = self
        textFieldScope.placeholderLabel.text = "Bán kính"
        textFieldScope.text = "30m"
        textFieldScope.inputView = scopePicker
        textFieldsScopeController = MDCTextInputControllerOutlined(textInput: textFieldScope)
        textFieldsScopeController?.helperText = "Yêu cầu *"
        textFieldsScopeController?.leadingUnderlineLabelTextColor = UIColor.red
        toolbar.sizeToFit()
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneScopePressed))
        toolbar.items = [btnDone]
        textFieldScope.inputAccessoryView = toolbar
    }
    
    private func createProvincesPicker(){
        let toolbar = UIToolbar()
        provincesPicker.tag = 1
        provincesPicker.dataSource = self
        provincesPicker.delegate = self
        textFieldsProvinces.placeholderLabel.text = "Tỉnh/Thành phố"
        textFieldsProvinces.inputView = provincesPicker
        textFieldProvinceController = MDCTextInputControllerOutlined(textInput: textFieldsProvinces)
        textFieldProvinceController?.helperText = "Yêu cầu *"
        textFieldProvinceController?.leadingUnderlineLabelTextColor = UIColor.red
        toolbar.sizeToFit()
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneProvincePressed))
        toolbar.items = [btnDone]
        textFieldsProvinces.inputAccessoryView = toolbar
    }
    
    private func createDistrictsPicker() {
        let toolbar = UIToolbar()
        districtsPicker.tag = 2
        districtsPicker.dataSource = self
        districtsPicker.delegate = self
        textFieldDistricts.isEnabled = false
        textFieldDistricts.placeholderLabel.text = "Quận/Huyện"
        textFieldDistricts.inputView = districtsPicker
        textFieldDistrictController = MDCTextInputControllerOutlined(textInput: textFieldDistricts)
        textFieldDistrictController?.helperText = "Yêu cầu *"
        textFieldDistrictController?.leadingUnderlineLabelTextColor = UIColor.red
        toolbar.sizeToFit()
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneDistrictsPressed))
        toolbar.items = [btnDone]
        textFieldDistricts.inputAccessoryView = toolbar
    }
    
    private func createWardsPicker() {
        let toolbar = UIToolbar()
        wardsPicker.tag = 3
        wardsPicker.dataSource = self
        wardsPicker.delegate = self
        textFieldWards.isEnabled = false
        textFieldWards.placeholderLabel.text = "Xã/Phường"
        textFieldWards.inputView = wardsPicker
        textFieldWardsController = MDCTextInputControllerOutlined(textInput: textFieldWards)
        textFieldWardsController?.helperText = "Yêu cầu *"
        textFieldWardsController?.leadingUnderlineLabelTextColor = UIColor.red
        toolbar.sizeToFit()
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneWardsPressed))
        toolbar.items = [btnDone]
        textFieldWards.inputAccessoryView = toolbar
    }
    
    private func getProvinces() {
        Alamofire.request("https://vapi.vnappmob.com/api/province", method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .responseObject { [self] (response: DataResponse<ProvincesResponse>)  in
                for i in 0..<(response.result.value?.results?.count)! {
                    provinces.append((response.result.value?.results![i])!)
                    self.provincesPicker.reloadAllComponents()
                }
            }
    }
    
    private func getDistricts(idProvince: String) {
        Alamofire.request("https://vapi.vnappmob.com/api/province/district/" + idProvince, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .responseObject { [self] (response: DataResponse<DistrictsResponse>)  in
                districts.removeAll()
                textFieldDistricts.text = nil
                for i in 0..<(response.result.value?.results?.count)! {
                    districts.append((response.result.value?.results![i])!)
                    self.districtsPicker.reloadAllComponents()
                    textFieldDistricts.isEnabled = true
                }
            }
    }
    
    private func getWards(idDistrict: String) {
        Alamofire.request("https://vapi.vnappmob.com/api/province/ward/" + idDistrict, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .responseObject { [self] (response: DataResponse<WardResponse>)  in
                wards.removeAll()
                textFieldWards.text = nil
                for i in 0..<(response.result.value?.results?.count)! {
                    wards.append((response.result.value?.results![i])!)
                    self.wardsPicker.reloadAllComponents()
                    textFieldWards.isEnabled = true
                }
            }
    }
    
    private func navigationToMapController(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let mapScreen = sb.instantiateViewController(identifier: "MapController")
            as! MapViewController
        mapScreen.mgs = textFieldMGS.text
        self.navigationController?.pushViewController(mapScreen, animated: true)
    }
    
    private func saveUser(
        phoneNumber: String,
        cmnd: String,
        fullname: String,
        radius: String,
        sex: String,
        province: String,
        district: String,
        ward: String,
        address: String,
        mgs: String
    ){
        let defaults = UserDefaults.standard
        defaults.setValue(phoneNumber, forKey: DefaultsKeys.phoneNumber)
        defaults.setValue(cmnd, forKey: DefaultsKeys.cmnd)
        defaults.setValue(fullname, forKey: DefaultsKeys.fullname)
        defaults.setValue(radius, forKey: DefaultsKeys.radius)
        defaults.setValue(sex, forKey: DefaultsKeys.sex)
        defaults.setValue(province, forKey: DefaultsKeys.province)
        defaults.setValue(district, forKey: DefaultsKeys.district)
        defaults.setValue(ward, forKey: DefaultsKeys.ward)
        defaults.setValue(address, forKey: DefaultsKeys.address)
        defaults.setValue("false", forKey: DefaultsKeys.registerUser)
    }
    
    @objc func doneScopePressed() {
        let scope = scopes[scopePicker.selectedRow(inComponent: 0)]
        textFieldScope.text = scope
        switch scope {
        case "30m":
            scopeSelect = "30"
        case "40m":
            scopeSelect = "40"
        case "50m":
            scopeSelect = "50"
        case "60m":
            scopeSelect = "60"
        default:
            scopeSelect = "30"
        }
        self.view.endEditing(true)
    }
    
    @objc func optionSelected(radioGroup: RadioGroup) {
        sexRadioGroupSelect = radioGroup.titles[radioGroup.selectedIndex] ?? ""
    }
    
    @objc func doneProvincePressed() {
        let province = provinces[provincesPicker.selectedRow(inComponent: 0)]
        textFieldsProvinces.text = province.province_name
        provinceSelect = province
        getDistricts(idProvince: province.province_id!)
        self.view.endEditing(true)
    }
    
    @objc func doneDistrictsPressed() {
        let district = districts[districtsPicker.selectedRow(inComponent: 0)]
        textFieldDistricts.text = district.district_name
        districtSelect = district
        getWards(idDistrict: district.district_id!)
        self.view.endEditing(true)
    }
    
    @objc func doneWardsPressed() {
        let ward = wards[wardsPicker.selectedRow(inComponent: 0)]
        textFieldWards.text = ward.ward_name
        wardSelect = ward
        self.view.endEditing(true)
    }
}

extension HomeViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhone {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
                }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 10
        }else if textField == textFieldCMND {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
                }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 12
        }

       return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == textFieldPhone {
            if (textFieldPhone.text?.isValidPhone() == true){
                textFieldsPhoneController?.setErrorText(nil, errorAccessibilityValue: nil)
            }else {
                textFieldsPhoneController?.setErrorText("Số ĐT phải có 10 chữ số và bắt đầu bằng 0", errorAccessibilityValue: nil)
            }
        }else if textField == textFieldCMND {
            if (textFieldCMND.text?.isValidCMND() == true){
                textFieldsCMNDController?.setErrorText(nil, errorAccessibilityValue: nil)
            }else {
                textFieldsCMNDController?.setErrorText("Số CMND phải là 9 hoặc 12 chữ số", errorAccessibilityValue: nil)
            }
        }else if textField == textFieldFullName {
            if (textFieldFullName.text?.isValidFullName() == true){
                textFieldsFullNameController?.setErrorText(nil, errorAccessibilityValue: nil)
            }else {
                textFieldsFullNameController?.setErrorText("Tên không có khoảng trắng đầu và cuối", errorAccessibilityValue: nil)
            }
        }
    }
}

extension HomeViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return scopes.count
        case 1:
            return provinces.count
        case 2:
            return districts.count
        case 3:
            return wards.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return scopes[row]
        case 1:
            return provinces[row].province_name
        case 2:
            return districts[row].district_name
        case 3:
            return wards[row].ward_name
        default:
            return nil
        }
    }
}

extension UIButton {
    func alignImageRight() {
        if UIApplication.shared.userInterfaceLayoutDirection == .leftToRight {
            semanticContentAttribute = .forceRightToLeft
        }
        else {
            semanticContentAttribute = .forceLeftToRight
        }
    }
}

extension String {
    func isValidPhone() -> Bool {
        let inputRegEx = "(^0\\d{9}$)"
        let inputpred = NSPredicate(format: "SELF MATCHES %@", inputRegEx)
        return inputpred.evaluate(with: self)
    }
    
    func isValidCMND() -> Bool {
        let inputRegEx = "(^\\d{9}$)|(^\\d{12}$)"
        let inputpred = NSPredicate(format: "SELF MATCHES %@", inputRegEx)
        return inputpred.evaluate(with: self)
    }
    
    func isValidFullName() -> Bool {
        let inputRegEx = "^\\S[\\w\\s]*\\S$"
        let inputpred = NSPredicate(format: "SELF MATCHES %@", inputRegEx)
        return inputpred.evaluate(with: self)
    }
    
    
}

