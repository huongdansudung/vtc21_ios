//
//  CustomMakerView.swift
//  LocationApp
//
//  Created by Mac on 02/09/2021.
//

import Foundation
import UIKit

class CustomMakerView: UIView {
    var img: UIImage!
    
    init(frame: CGRect, image: UIImage, tag: Int) {
        super.init(frame: frame)
        self.img = image
        self.tag = tag
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        let imageView = UIImageView(image: img)
        imageView.frame = CGRect(x: 10, y: 10, width: 50, height: 50)
        imageView.clipsToBounds = true
        let lbl = UILabel(frame: CGRect(x: 0, y: -18, width: 70, height: 50))
        lbl.text = "Vùng cách ly"
        lbl.font = UIFont.systemFont(ofSize: 10)
        lbl.textColor = UIColor.red
        self.addSubview(lbl)
        self.addSubview(imageView)
    }
}
