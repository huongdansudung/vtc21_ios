//
//  MapViewController.swift
//  LocationApp
//
//  Created by Mac on 30/08/2021.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Lottie

class MapViewController: UIViewController {
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var preciseLocationZoomLevel: Float = 15.0
    var approximateLocationZoomLevel: Float = 10.0
    var locationSelect: String = ""
    var isFirst = false
    var defaults = UserDefaults.standard
    var mgs: String!
    var lastStatus: String!
    var animationView = AnimationView()
    
    @IBOutlet weak var mapContainer: GMSMapView!
    @IBOutlet weak var containerTitle: UIView!
    @IBOutlet weak var animationService: UIView!
    @IBOutlet weak var actionRegister: UIButton!
    @IBOutlet weak var titleAnimation: UILabel!
    @IBOutlet weak var iconCenterMaker: UIImageView!
    
    private var countDanger = 0
    private var isRunning = false
    var timerCallF: Timer!
    
    //test
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    private let client = GMSPlacesClient.shared()
    
    let titleService: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = .systemFont(ofSize: 17)
            label.textColor = .white
            label.text = "Xác nhận vị trí thành công. Hệ thống vẫn tiếp tục lưu thông tin khi ứng dụng chạy nền."
            return label
        }()
    
    let btnMyLocation: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.white
        btn.setImage(UIImage(named: "MyLocation"), for: .normal)
        btn.clipsToBounds = true
        btn.tintColor = UIColor.gray
        btn.imageView?.tintColor = UIColor.gray
        btn.addTarget(self, action: #selector(btnMyLocationAction), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let btnSetting: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.white
        btn.setImage(UIImage(named: "Setting"), for: .normal)
        btn.clipsToBounds = true
        btn.tintColor = UIColor.gray
        btn.imageView?.tintColor = UIColor.gray
        btn.addTarget(self, action: #selector(btnSettingAction), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        setUpViews()
        createTitleService()
        createAnimation()
//        if (defaults.string(forKey: DefaultsKeys.registerUser) == "true") {
//            registerUserSuccess()
//        }
    
        //Test
        let filter = GMSAutocompleteFilter()
        filter.country = "VN"
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.autocompleteFilter = filter
        resultsViewController?.delegate = self
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        searchController?.searchBar.sizeToFit()
        searchController?.searchBar.placeholder = "Tọa độ người cách ly"
        self.navigationItem.searchController = searchController
        definesPresentationContext = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("chạy vào")
        if (defaults.string(forKey: DefaultsKeys.registerUser) == "true") {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            createTitleService()
            registerUserSuccess()
        }
    }
    
    @IBAction func actionRegisterUser(_ sender: Any) {
        if (locationSelect != "") {
            DispatchQueue.main.asyncDeduped(target: self, after: 0.5, execute: { [self] in
                let defaults = UserDefaults.standard
                MGConnection.request(APIRouter.createUser(id: "0",
                                                          name: defaults.string(forKey: DefaultsKeys.fullname)!,
                                                          cmnd: defaults.string(forKey: DefaultsKeys.cmnd)!,
                                                          adress: defaults.string(forKey: DefaultsKeys.address)!,
                                                          tinh: defaults.string(forKey: DefaultsKeys.province)!,
                                                          quan: defaults.string(forKey: DefaultsKeys.district)!,
                                                          phuong: defaults.string(forKey: DefaultsKeys.ward)!,
                                                          telephone: defaults.string(forKey: DefaultsKeys.phoneNumber)!,
                                                          location: locationSelect,
                                                          rarius: defaults.string(forKey: DefaultsKeys.radius)!,
                                                          mags: mgs,
                                                          is_lock: "0",
                                                          is_reset: "0"
                ), CreateUserResponse.self, completion: {(result, err) in
                    guard err == nil else {
                        Toast.show(message: (err?.mErrorMessage)!, controller: self)
                        return
                    }
                    if result?.message! == "Ok" {
                        defaults.setValue(locationSelect.split(separator: ",")[0], forKey: DefaultsKeys.locationLatitude)
                        defaults.setValue(locationSelect.split(separator: ",")[1], forKey: DefaultsKeys.locationLongitude)
                        defaults.setValue(result?.user!.id, forKey: DefaultsKeys.idUser)
                        registerUserSuccess()
                        Toast.show(message: "Đăng ký thành công", controller: self)
                    }else {
                        Toast.show(message: "Đăng ký không thành công", controller: self)
                    }
                })
            })
        }
    }
    
    private func initView() {
        title = "Đang tải..."
        
        // Kích hoạt nhận biết app mở trở lại từ nền
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.showsBackgroundLocationIndicator = true
        placesClient = GMSPlacesClient.shared()
        
        // Vị trí mặc định để sử dụng khi quyền vị trí không được cấp
        let defaultLocation = CLLocation(latitude: -33.869405, longitude: 151.199)
        
        // Tạo map
        let zoomLevel = locationManager.accuracyAuthorization == .fullAccuracy ? preciseLocationZoomLevel : approximateLocationZoomLevel
        let camera = GMSCameraPosition.camera(withLatitude: defaultLocation.coordinate.latitude,
                                              longitude: defaultLocation.coordinate.longitude,
                                              zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: mapContainer.bounds, camera: camera)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        mapContainer.addSubview(mapView)
        mapView.isHidden = true
    }
    
    private func setUpViews() {
        self.view.addSubview(btnMyLocation)
        btnMyLocation.topAnchor.constraint(equalTo: containerTitle.bottomAnchor, constant: 10).isActive = true
        btnMyLocation.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        btnMyLocation.widthAnchor.constraint(equalToConstant: 50).isActive = true
        btnMyLocation.heightAnchor.constraint(equalTo: btnMyLocation.widthAnchor).isActive = true
        
        self.view.addSubview(btnSetting)
        btnSetting.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        btnSetting.widthAnchor.constraint(equalToConstant: 50).isActive = true
        btnSetting.heightAnchor.constraint(equalTo: btnSetting.widthAnchor).isActive = true
        btnSetting.centerYAnchor.constraint(equalTo: actionRegister.centerYAnchor).isActive = true
        btnSetting.isHidden = true
    }
 
    private func reverseGeocode(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard
                let address = response?.firstResult(),
                let lines = address.lines
            else {
                return
            }
            self.title = lines.joined(separator: "\n")
        }
    }
    
    private func promptForAuthorization() {
        let alert = UIAlertController(title: "Truy cập vị trí là cần thiết để lấy được vị trí hiện tại của bạn",
                                      message: "Làm ơn hãy cho phép truy cập vị trí",
                                      preferredStyle: .alert
        )
        let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: {_ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                      options: [:],
                                      completionHandler: nil
            )
            self.navigationController?.popViewController(animated: true)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        })
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        alert.preferredAction = settingsAction
        present(alert, animated: true, completion: nil)
    }
    
    private func createTitleService() {
        let centerY = NSLayoutConstraint(item: titleService,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: containerTitle,
                                         attribute: .centerY,
                                         multiplier: 1,
                                         constant: 0)
        containerTitle.addSubview(titleService)
        containerTitle.addConstraint(centerY)
        startAnimation()
    }
    
    private func createAnimation() {
        animationView = AnimationView()
        animationView = .init(name: "protection")
        animationService.backgroundColor = .clear
        animationView.loopMode = .loop
        animationView.frame = animationService.bounds
        animationView.contentMode = .scaleAspectFit
        animationView.backgroundBehavior = .pauseAndRestore
        animationService.addSubview(animationView)
        animationView.play()
    }
    
    func startAnimation() {
        //Animating the label automatically change as per your requirement
        DispatchQueue.main.async(execute: {
            UIView.animate(withDuration: 10.0, delay: 1, options: ([.curveLinear, .repeat]), animations: {
                () -> Void in
                self.titleService.center = CGPoint(x: 0 - self.titleService.bounds.size.width / 2, y: self.titleService.center.y)
            }, completion:  nil)
        })
    }
    
    func monitorRegionAtLocation(center: CLLocationCoordinate2D, identifier: String, radius: Double) {
        // Make sure the devices supports region monitoring.
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            // Register the region.
            let region = CLCircularRegion(center: center, radius: radius, identifier: identifier)
 
            locationManager.startMonitoring(for: region)
        }
    }
    
    private func registerUserSuccess() {
        // Thêm maker tọa độ đăng ký người dùng
        self.mapView.clear()
        let position = CLLocationCoordinate2D(latitude: Double(defaults.string(forKey: DefaultsKeys.locationLatitude)!)!,
                                              longitude: Double(defaults.string(forKey: DefaultsKeys.locationLongitude)!)!)
        let marker = GMSMarker(position: position)
        let customMakerView = CustomMakerView(frame: CGRect(x: 0,
                                                                  y: 0,
                                                                  width: 70,
                                                                  height: 60),
                                                    image: UIImage(named: "marker_covit")!,
                                                    tag: 0)
        marker.iconView = customMakerView
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.map = self.mapView
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.containerTitle.isHidden = false
        self.animationService.isHidden = false
        self.actionRegister.isHidden = true
        self.titleAnimation.isHidden = false
        self.iconCenterMaker.isHidden = true
        self.btnSetting.isHidden = false
        defaults.setValue("true", forKey: DefaultsKeys.registerUser)
    }
    
    func startTimer() {
        guard timerCallF == nil else {
            return
        }
        timerCallF = Timer.scheduledTimer(withTimeInterval: 60, repeats: false) { (timer) in
            self.isRunning = false
        }
    }
    
    func stopTimer() {
        timerCallF?.invalidate()
        timerCallF = nil
    }
    
    func moveCamera(location: CLLocation) {
        let zoomLevel = locationManager.accuracyAuthorization == .fullAccuracy ? preciseLocationZoomLevel : approximateLocationZoomLevel
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        self.isFirst = true
    }
    
    private func navigationToMapRegisterController() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let mapRegisterScreen = sb.instantiateViewController(identifier: "MapRegister") as! MapRegisterViewController
//        present(mapRegisterScreen, animated: true, completion: nil)
        self.navigationController?.pushViewController(mapRegisterScreen, animated: true)
    }
    
    @objc func appMovedToBackground() {
        createTitleService()
    }
    
    @objc func btnMyLocationAction() {
        let location: CLLocation? = mapView.myLocation
        if location != nil {
            mapView.animate(toLocation: (location?.coordinate)!)
        }
    }
    
    @objc func btnSettingAction() {
        navigationToMapRegisterController()
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    // Xử lý các sự kiện vị trí trả về
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (self.isFirst == false) {
            let location: CLLocation = locations.last!
            self.moveCamera(location: location)
        }else {
            if (defaults.string(forKey: DefaultsKeys.registerUser) == "true") {
                MGConnection.request(APIRouter.requestLocation(id: defaults.string(forKey: DefaultsKeys.idUser)!,
                                                               name: "string",
                                                               cmnd: "string",
                                                               adress: "String",
                                                               tinh: "String",
                                                               quan: "String",
                                                               phuong: "String",
                                                               telephone: "String",
                                                               location: "\(locations.last!.coordinate.latitude),\(locations.last!.coordinate.longitude)",
                                                               rarius: "0",
                                                               mags: "String",
                                                               is_lock: "0",
                                                               is_reset: "0"),
                                     CreateUserResponse.self,
                                     completion: {(result, err) in
                                        guard err == nil else {
                                            Toast.show(message: (err?.mErrorMessage)!, controller: self)
                                            return
                                        }
                                        DispatchQueue.main.async { [self] in
                                            updateStatus(status: (result?.message)!)
                                            callFo(status: (result?.message)!)
                                        }
                                    })
            }
        }

    }
    
    private func callFo(status: String) {
        switch status {
        case "Danger":
            self.countDanger = self.countDanger + 1
            if (self.countDanger >= 15 && self.isRunning == false) {
                // call f0
                MGConnection.request(APIRouter.callFo(id: defaults.string(forKey: DefaultsKeys.idUser)!),
                                     ResetUserResponse.self,
                                     completion: {(result, err) in
                                        guard err == nil else {
                                            Toast.show(message: (err?.mErrorMessage)!, controller: self)
                                            print("Call F0 result: \(err?.mErrorMessage ?? "Error")")
                                            return
                                        }
                                        DispatchQueue.main.async {
                                            print("Call F0 result: \(result!.message ?? "Error")")
                                        }
                                    })
                
                //reset 3p
                self.startTimer()
                self.isRunning = true
            }
        default:
            self.countDanger = 0
            if (self.isRunning) {
                self.isRunning = false
                self.stopTimer()
            }
        }
    }
    
    private func updateStatus(status: String) {
        print(status)
        if (status == "Safe" && status != self.lastStatus) {
            self.titleAnimation.text = "Trong vùng cách ly"
            self.titleAnimation.textColor = UIColor.green

            animationView.removeFromSuperview()
            animationView = AnimationView()
            animationView = .init(name: "protection")
            animationService.backgroundColor = .clear
            animationView.loopMode = .loop
            animationView.frame = animationService.bounds
            animationView.contentMode = .scaleAspectFit
            animationView.backgroundBehavior = .pauseAndRestore
            animationService.addSubview(animationView)
            animationView.play()
            
            lastStatus = "Safe"
        }else if (status == "Danger" && status != self.lastStatus) {
            self.titleAnimation.text = "Ngoài vùng cách ly"
            self.titleAnimation.textColor = UIColor.red
            
            animationView.removeFromSuperview()
            animationView = AnimationView()
            animationView = .init(name: "warning")
            animationService.backgroundColor = .clear
            animationView.loopMode = .loop
            animationView.frame = animationService.bounds
            animationView.contentMode = .scaleAspectFit
            animationView.backgroundBehavior = .pauseAndRestore
            animationService.addSubview(animationView)
            animationView.play()
            
            lastStatus = "Danger"
        }else if (status == "lock") {
            locationManager.stopUpdatingLocation()
            self.titleAnimation.text = "Bạn không còn thuộc diện cách ly"
            self.titleService.text = "Hiện tại bạn không còn được giám sát cách ly."
            self.titleAnimation.textColor = UIColor.green
            
            animationView.removeFromSuperview()
            animationView = AnimationView()
            animationView = .init(name: "protection")
            animationService.backgroundColor = .clear
            animationView.loopMode = .loop
            animationView.frame = animationService.bounds
            animationView.contentMode = .scaleAspectFit
            animationView.backgroundBehavior = .pauseAndRestore
            animationService.addSubview(animationView)
            animationView.play()
            
            locationManager.stopUpdatingLocation()
            let alert = UIAlertController(title: "Thông báo",
                                          message: "Tài khoản đã được tắt giám sát.",
                                          preferredStyle: .alert)
            let agreeAction = UIAlertAction(title: "Đồng ý", style: .default, handler: { [weak self] _ in
                self?.navigationController?.popToRootViewController(animated: true)
                self!.defaults.setValue("false", forKey: DefaultsKeys.registerUser)
            })
            alert.addAction(agreeAction)
            alert.preferredAction = agreeAction
            present(alert, animated: true, completion: nil)
            
        }else if (status == "Reset") {
            locationManager.stopUpdatingLocation()
            let alert = UIAlertController(title: "Thông báo",
                                          message: "Tài khoản của bạn đã được làm mới thông tin - Vui lòng đăng ký lại thông tin.Chi tiết liên hệ người giám sát.",
                                          preferredStyle: .alert)
            let agreeAction = UIAlertAction(title: "Đồng ý", style: .default, handler: { [weak self] _ in
                self?.navigationController?.popToRootViewController(animated: true)
                self!.defaults.setValue("false", forKey: DefaultsKeys.registerUser)
            })
            alert.addAction(agreeAction)
            alert.preferredAction = agreeAction
            present(alert, animated: true, completion: nil)
        }
    }
    
    // Xử lý ủy quyền cho người quản lý vị trí
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // Kiểm tra mức độ chính xác
        let accuracy = manager.accuracyAuthorization
        switch accuracy {
        case .fullAccuracy:
            print("Vị trí chính xác.")
        case .reducedAccuracy:
            print("Độ chính xác của vị trí không chính xác.")
        @unknown default:
            fatalError()
        }
        
        // Xử lý trạng thái ủy quyền
        switch status {
        case .restricted:
            print("Quyền vị trí đã bị hạn chế.")
        case .denied:
            print("Người dùng từ chối quyền truy cập vào vị trí.")
             // Hiển thị bản đồ bằng cách sử dụng vị trí mặc định
            mapView.isHidden = false
            promptForAuthorization()
        case .notDetermined:
            print("Trạng thái vị trí không được xác định.")
        case .authorizedAlways:
            print("Quyền trạng thái được đồng ý luôn luôn.")
            locationManager.startUpdatingLocation()
        case .authorizedWhenInUse:
            print("Quyền trạng thái được đồng ý khi sử dụng ứng dụng.")
            locationManager.startUpdatingLocation()
        @unknown default:
            fatalError()
        }
    }
    
    // Xử lý lỗi trình quản lý vị trí
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error).")
    }
    
    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
        print("Chạy didFinishDeferredUpdatesWithError")
    }
    
    @objc func request() {
        locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
    }
    
}

extension MapViewController: GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.locationSelect = "\(position.target.latitude),\(position.target.longitude)"
        reverseGeocode(coordinate: position.target)
    }
}

extension MapViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                             didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        let location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        self.moveCamera(location: location)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                             didFailAutocompleteWithError error: Error){
        // xử lý lỗi
        print("Error: ", error.localizedDescription)
    }
    
}
