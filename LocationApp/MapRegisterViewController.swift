//
//  MapRegisterViewController.swift
//  LocationApp
//
//  Created by Mac on 06/09/2021.
//

import UIKit
import GoogleMaps
import GooglePlaces

@available(iOS 14.0, *)
class MapRegisterViewController: UIViewController {
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var imageViewLocationCenter: UIImageView!
    
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var placesClient: GMSPlacesClient!
    var preciseLocationZoomLevel: Float = 15.0
    var approximateLocationZoomLevel: Float = 10.0
    var locationSelect: String = ""
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    private let client = GMSPlacesClient.shared()
    var defaults = UserDefaults.standard
    
    let btnMyLocation: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.white
        btn.setImage(UIImage(named: "MyLocation"), for: .normal)
        btn.clipsToBounds = true
        btn.tintColor = UIColor.gray
        btn.imageView?.tintColor = UIColor.gray
        btn.addTarget(self, action: #selector(btnMyLocationAction), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        setUpViews()
        setUpSearchView()
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        print("vô")
        MGConnection.request(APIRouter.editLocation(
                                id: defaults.string(forKey: DefaultsKeys.idUser)!,
                                location: locationSelect),
                             CreateUserResponse.self,
                             completion: {(result, err) in
                             guard err == nil else {
                                 Toast.show(message: (err?.mErrorMessage)!, controller: self)
                                 return
                             }
                                DispatchQueue.main.async { [self] in
                                if result?.message == "Ok" {
                                    defaults.setValue(locationSelect.split(separator: ",")[0], forKey: DefaultsKeys.locationLatitude)
                                    defaults.setValue(locationSelect.split(separator: ",")[1], forKey: DefaultsKeys.locationLongitude)
                                    self.navigationController?.popViewController(animated: true)
                                }else {
                                    Toast.show(message: "Thay đổi tọa độ thất bại.", controller: self)
                                }
                             }
                         })
    }
    
    private func initView() {
        title = "Đang tải..."
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        placesClient = GMSPlacesClient.shared()
        
        // Vị trí mặc định để sử dụng khi quyền vị trí không được cấp
        let defaultLocation = CLLocation(latitude: -33.869405, longitude: 151.199)
        
        // Tạo map
        let zoomLevel = locationManager.accuracyAuthorization == .fullAccuracy ? preciseLocationZoomLevel : approximateLocationZoomLevel
        let camera = GMSCameraPosition.camera(withLatitude: defaultLocation.coordinate.latitude,
                                              longitude: defaultLocation.coordinate.longitude,
                                              zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: mapContainer.bounds, camera: camera)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        mapContainer.addSubview(mapView)
        mapView.isHidden = true
    }
    
    private func setUpViews() {
        self.view.addSubview(btnMyLocation)
        btnMyLocation.topAnchor.constraint(equalTo: mapContainer.topAnchor, constant: 16).isActive = true
        btnMyLocation.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        btnMyLocation.widthAnchor.constraint(equalToConstant: 50).isActive = true
        btnMyLocation.heightAnchor.constraint(equalTo: btnMyLocation.widthAnchor).isActive = true
    }
    
    private func setUpSearchView() {
        let filter = GMSAutocompleteFilter()
        filter.country = "VN"
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.autocompleteFilter = filter
        resultsViewController?.delegate = self
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        searchController?.searchBar.sizeToFit()
        searchController?.searchBar.placeholder = "Tọa độ người cách ly"
        self.navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func moveCamera(location: CLLocation) {
        let zoomLevel = locationManager.accuracyAuthorization == .fullAccuracy ? preciseLocationZoomLevel : approximateLocationZoomLevel
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
    }
    
    private func promptForAuthorization() {
        let alert = UIAlertController(title: "Truy cập vị trí là cần thiết để lấy được vị trí hiện tại của bạn",
                                      message: "Làm ơn hãy cho phép truy cập vị trí",
                                      preferredStyle: .alert
        )
        let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: {_ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                      options: [:],
                                      completionHandler: nil
            )
            self.navigationController?.popViewController(animated: true)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        })
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        alert.preferredAction = settingsAction
        present(alert, animated: true, completion: nil)
    }
    
    @objc func btnMyLocationAction() {
        let location: CLLocation? = mapView.myLocation
        if location != nil {
            mapView.animate(toLocation: (location?.coordinate)!)
        }
    }
}

extension MapRegisterViewController: CLLocationManagerDelegate {
    // Xử lý các sự kiện vị trí trả về
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        self.moveCamera(location: location)
    }
    
    // Xử lý ủy quyền cho người quản lý vị trí
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // Kiểm tra mức độ chính xác
        let accuracy = manager.accuracyAuthorization
        switch accuracy {
        case .fullAccuracy:
            print("Vị trí chính xác.")
        case .reducedAccuracy:
            print("Độ chính xác của vị trí không chính xác.")
        @unknown default:
            fatalError()
        }
        
        // Xử lý trạng thái ủy quyền
        switch status {
        case .restricted:
            print("Quyền vị trí đã bị hạn chế.")
        case .denied:
            print("Người dùng từ chối quyền truy cập vào vị trí.")
             // Hiển thị bản đồ bằng cách sử dụng vị trí mặc định
            mapView.isHidden = false
            promptForAuthorization()
        case .notDetermined:
            print("Trạng thái vị trí không được xác định.")
        case .authorizedAlways:
            print("Quyền trạng thái được đồng ý luôn luôn.")
            locationManager.startUpdatingLocation()
        case .authorizedWhenInUse:
            print("Quyền trạng thái được đồng ý khi sử dụng ứng dụng.")
            locationManager.startUpdatingLocation()
        @unknown default:
            fatalError()
        }
    }
    
    // Xử lý lỗi trình quản lý vị trí
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error).")
    }
}

extension MapRegisterViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                             didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        let location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        self.moveCamera(location: location)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                             didFailAutocompleteWithError error: Error){
        // xử lý lỗi
        print("Error: ", error.localizedDescription)
    }
    
}

extension MapRegisterViewController: GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.locationSelect = "\(position.target.latitude),\(position.target.longitude)"
        reverseGeocode(coordinate: position.target)
    }
    
    private func reverseGeocode(coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard
                let address = response?.firstResult(),
                let lines = address.lines
            else {
                return
            }
            self.title = lines.joined(separator: "\n")
        }
    }
}
