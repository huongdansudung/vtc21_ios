//
//  MapCheckerViewController.swift
//  LocationApp
//
//  Created by Mac on 04/09/2021.
//

import UIKit
import GoogleMaps

class MapCheckerViewController: UIViewController {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var LogsTableview: UITableView!
    var preciseLocationZoomLevel: Float = 16.0
    var approximateLocationZoomLevel: Float = 10.0
    var map: GMSMapView!
    var idUser: String!
    var username: String!
    var logs: [Log]!
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        getLogs(id: idUser)
    }
    
    private func initView() {
        title = "Lịch sử di chuyển - \(username ?? "Error")"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let defaultLocation = CLLocation(latitude: -33.869405, longitude: 151.199)
        let camera = GMSCameraPosition.camera(withLatitude: defaultLocation.coordinate.latitude,
                                              longitude: defaultLocation.coordinate.longitude,
                                              zoom: preciseLocationZoomLevel)
        map = GMSMapView.map(withFrame: mapView.bounds, camera: camera)
        map.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.addSubview(map)
        map.isHidden = true
        
        refreshControl.attributedTitle = NSAttributedString(string: "Làm mới")
        LogsTableview.dataSource = self
        LogsTableview.delegate = self
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        LogsTableview.refreshControl = refreshControl
        LogsTableview.addSubview(refreshControl)
    }
    
    @objc func refresh() {
        getLogs(id: self.idUser)
    }
    
    private func getLogs(id: String) {
        MGConnection.request(APIRouter.getLogs(id: id),
                             LogsResponse.self,
                             completion: {(result, err) in
                                guard err == nil else {
                                    Toast.show(message: (err?.mErrorMessage)!, controller: self)
                                    return
                                }
                                DispatchQueue.main.async { [self] in
                                    if !(result?.logs!.isEmpty)! {
                                        self.map.clear()
                                        let camera = GMSCameraPosition.camera(
                                            withLatitude: Double((result?.logs![0].location?.split(separator: ",")[0])!)!,
                                            longitude: Double((result?.logs![0].location?.split(separator: ",")[1])!)!,
                                            zoom: self.preciseLocationZoomLevel)
                                        if map.isHidden {
                                            map.isHidden = false
                                            map.camera = camera
                                        } else {
                                            map.animate(to: camera)
                                        }
                                        
                                        // add maker
                                        let position = CLLocationCoordinate2D(
                                            latitude: Double((result?.logs![0].location?.split(separator: ",")[0])!)!,
                                            longitude: Double((result?.logs![0].location?.split(separator: ",")[1])!)!
                                        )
                                        let marker = GMSMarker(position: position)
                                        marker.icon = UIImage(named: "placeholder")
                                        marker.map = self.map
                                        
                                        //set data mytableview
                                        self.logs = result?.logs
                                        self.LogsTableview.reloadData()
                                        refreshControl.endRefreshing()
                                   }else {
                                       Toast.show(message: "Lock thất bại.", controller: self)
                                   }
                                }
                            })
    }
    
}

extension MapCheckerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if logs == nil {
            return 0
        }else {
            return logs!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMap", for: indexPath) as! LogsTableViewCell
        cell.labelDate.text = "\(self.logs![indexPath.row].date_chance?.split(separator: "T")[0] ?? "Error")"
        cell.labelTime.text = "\(self.logs![indexPath.row].date_chance?.split(separator: "T")[1].split(separator: ".")[0] ?? "Error")"
        cell.labelLog.text = self.logs![indexPath.row].log
        return cell
    }
    
    
}
