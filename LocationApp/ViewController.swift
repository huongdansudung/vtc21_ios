//
//  ViewController.swift
//  LocationApp
//
//  Created by Mac on 16/08/2021.
//

import UIKit

class ViewController: UIViewController {
    var defaults = UserDefaults.standard
    
    @IBOutlet weak var buttonChecker: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonChecker.layer.borderWidth = 2
        buttonChecker.layer.borderColor = UIColor.orange.cgColor
        if (defaults.string(forKey: DefaultsKeys.registerUser) == "true") {
            navigationToMapController()
        }
        if (defaults.string(forKey: DefaultsKeys.mgs) != "" &&
                defaults.string(forKey: DefaultsKeys.mgs) != nil) {
            navigationToUserController()
        }
    }
    
    @IBAction func actionUser(_ sender: Any) {
        navigationToHomeController()
    }
    @IBAction func actionLogin(_ sender: Any) {
        navigationToLoginController()
    }
    
    private func navigationToHomeController(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let homeScreen = sb.instantiateViewController(identifier: "HomeViewController")
            as! HomeViewController
        self.navigationController?.pushViewController(homeScreen, animated: true)
    }
    
    private func navigationToMapController(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let mapScreen = sb.instantiateViewController(identifier: "MapController")
            as! MapViewController
        self.navigationController?.pushViewController(mapScreen, animated: true)
    }
    
    private func navigationToLoginController() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let loginScreen = sb.instantiateViewController(identifier: "LoginScreen") as! LoginViewController
        self.navigationController?.pushViewController(loginScreen, animated: true)
    }
    
    private func navigationToUserController() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let userScreen = sb.instantiateViewController(identifier: "UserChecker") as! UsersViewController
        self.navigationController?.pushViewController(userScreen, animated: true)
    }
    
}

extension DispatchQueue {
    public func asyncDeduped(target: AnyObject, after delay: TimeInterval, execute work: @escaping @convention(block) () -> Void) {
        let dedupeIdentifier = DispatchQueue.dedupeIdentifierFor(target)
        if let existingWorkItem = DispatchQueue.workItems.removeValue(forKey: dedupeIdentifier) {
            existingWorkItem.cancel()
            NSLog("Deduped work item: \(dedupeIdentifier)")
        }
        let workItem = DispatchWorkItem {
            DispatchQueue.workItems.removeValue(forKey: dedupeIdentifier)

            for ptr in DispatchQueue.weakTargets.allObjects {
                if dedupeIdentifier == DispatchQueue.dedupeIdentifierFor(ptr as AnyObject) {
                    work()
                    NSLog("Ran work item: \(dedupeIdentifier)")
                    break
                }
            }
        }

        DispatchQueue.workItems[dedupeIdentifier] = workItem
        DispatchQueue.weakTargets.addPointer(Unmanaged.passUnretained(target).toOpaque())

        asyncAfter(deadline: .now() + delay, execute: workItem)
    }
}

private extension DispatchQueue {

    static var workItems = [AnyHashable : DispatchWorkItem]()

    static var weakTargets = NSPointerArray.weakObjects()

    static func dedupeIdentifierFor(_ object: AnyObject) -> String {
        return "\(Unmanaged.passUnretained(object).toOpaque())." + String(describing: object)
    }

}
